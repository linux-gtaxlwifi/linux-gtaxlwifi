// SPDX-License-Identifier: GPL-2.0
/*
 * Samsung Exynos7870 SoC pin-mux and pin-config device tree source
 *
 * Copyright (c) ???
 */

#include <dt-bindings/interrupt-controller/arm-gic.h>
#include "exynos-pinctrl.h"

&pinctrl0 {
	etc0: etc0 {
		gpio-controller;
		#gpio-cells = <2>;
		
		interrupt-controller;
		#interrupt-cells = <2>;
	};

	etc1: etc1 {
		gpio-controller;
		#gpio-cells = <2>;
		
		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpa0: gpa0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
		interrupt-parent = <&gic>;
		interrupts = <GIC_SPI 0 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 1 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 2 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 3 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 4 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 5 IRQ_TYPE_LEVEL_HIGH>,
		             <GIC_SPI 6 IRQ_TYPE_LEVEL_HIGH>,
		             <GIC_SPI 7 IRQ_TYPE_LEVEL_HIGH>;
	};

	gpa1: gpa1 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
		interrupt-parent = <&gic>;
		interrupts = <GIC_SPI 8 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 9 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 10 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 11 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 12 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 13 IRQ_TYPE_LEVEL_HIGH>,
		             <GIC_SPI 14 IRQ_TYPE_LEVEL_HIGH>,
		             <GIC_SPI 15 IRQ_TYPE_LEVEL_HIGH>;
	};

	gpa2: gpa2 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpq0: gpq0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	uart2-bus {
		samsung,pins = "gpa1-1","gpa1-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	uart2-sleep {
		samsung,pins = "gpa1-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
	};

	bt-btwake {
		samsung,pins = "gpa1-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	bt-hostwake {
		samsung,pins = "gpa1-6";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_INPUT>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	gnss-sensor-irq {
		samsung,pins = "gpa2-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_6>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
	};

	gnss-sensor-i2c {
		samsung,pins = "gpa2-5","gpa2-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_6>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	nfc-int {
		samsung,pins = "gpa2-6";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
	};
};

&pinctrl1 {
	gpz0: gpz0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpz1: gpz1 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	i2s-bt-bus {
		samsung,pins = "gpz0-0", "gpz0-1", "gpz0-2", "gpz0-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	i2s-bt-bus-idle {
		samsung,pins = "gpz0-0", "gpz0-1", "gpz0-2", "gpz0-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	i2s-pmic-bus {
		samsung,pins = "gpz1-0", "gpz1-2", "gpz1-3", "gpz1-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR6>;
	};

	i2s-pmic-bus-idle {
		samsung,pins = "gpz1-0", "gpz1-2", "gpz1-3", "gpz1-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR6>;
	};

	i2s-pmic-amp-bus {
		samsung,pins = "gpz1-0", "gpz1-1", "gpz1-2", "gpz1-3", "gpz1-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR6>;
	};

	i2s-pmic-amp-bus-idle {
		samsung,pins = "gpz1-0", "gpz1-1", "gpz1-2", "gpz1-3", "gpz1-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR6>;
	};

	i2s-amp-bus {
		samsung,pins = "gpz1-5";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	i2s-amp-bus-idle {
		samsung,pins = "gpz1-5";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	i2s-fm-bus {
		samsung,pins = "gpz2-0", "gpz2-1", "gpz2-2", "gpz2-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	i2s-fm-bus-idle {
		samsung,pins = "gpz2-0", "gpz2-1", "gpz2-2", "gpz2-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};
};

&pinctrl2 {
	gpc7: gpc7 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	spi1-bus {
		samsung,pins = "gpc7-3", "gpc7-2", "gpc7-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	spi1-cs-0 {
		samsung,pins = "gpc7-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	spi1-cs-1 {
		samsung,pins = "gpc7-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};

&pinctrl3 {
	gpr0: gpr0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpr1: gpr1 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpr4: gpr4 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
		interrupt-parent = <&gic>;
		interrupts = <GIC_SPI 0 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 1 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 2 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 3 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 4 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 5 IRQ_TYPE_LEVEL_HIGH>;
	};

	sd0-clk {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-cmd {
		samsung,pins = "gpr0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-rdqs {
		samsung,pins = "gpr0-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-bus-width1 {
		samsung,pins = "gpr1-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-bus-width4 {
		samsung,pins = "gpr1-1","gpr1-2", "gpr1-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-bus-width8 {
		samsung,pins = "gpr1-4","gpr1-5", "gpr1-6", "gpr1-7";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-clk-fast-slew-rate-1x {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	sd0-clk-fast-slew-rate-2x {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR2>;
	};

	sd0-clk-fast-slew-rate-3x {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd0-clk-fast-slew-rate-4x {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	sd0-clk-fast-slew-rate-5x {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR5>;
	};

	sd0-clk-fast-slew-rate-6x {
		samsung,pins = "gpr0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR6>;
	};

	sd1_clk: sd1-clk {
		samsung,pins = "gpr2-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd1_cmd: sd1-cmd {
		samsung,pins = "gpr2-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd1_bus1: sd1-bus-width1 {
		samsung,pins = "gpr3-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_INPUT>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd1_bus4: sd1-bus-width4 {
		samsung,pins = "gpr3-1","gpr3-2", "gpr3-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_INPUT>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd2_clk: sd2-clk {
		samsung,pins = "gpr4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd2_cmd: sd2-cmd {
		samsung,pins = "gpr4-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd2_bus1: sd2-bus-width1 {
		samsung,pins = "gpr4-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd2_bus4: sd2-bus-width4 {
		samsung,pins = "gpr4-3","gpr4-4", "gpr4-5";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd2-clk-fast-slew-rate-1x {
		samsung,pins = "gpr4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	sd2-clk-fast-slew-rate-2x {
		samsung,pins = "gpr4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR2>;
	};

	sd2-clk-fast-slew-rate-3x {
		samsung,pins = "gpr4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	sd2-clk-fast-slew-rate-4x {
		samsung,pins = "gpr4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};
};

&pinctrl4 {
	gpm0: gpm0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	hs-i2c0-bus {
		samsung,pins = "gpm0-1","gpm0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};

&pinctrl5 {
	gpc2: gpc2 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	i2c2_bus: i2c2-bus {
		samsung,pins = "gpc2-1","gpc2-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	gpio-nfc {
		samsung,pins = "gpc2-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
		samsung,pin-val = <1>;
	};

	nfc-pd {
		samsung,pins = "gpc2-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	nfc-n5-clk-req {
		samsung,pins = "gpc2-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};

&pinctrl6 {
	gpb0: gpb0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc0: gpc0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc1: gpc1 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc4: gpc4 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc5: gpc5 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc6: gpc6 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc8: gpc8 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpc9: gpc9 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpd1: gpd1 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpd2: gpd2 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpd3: gpd3 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpd4: gpd4 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpe0: gpe0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpf0: gpf0 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpf1: gpf1 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpf2: gpf2 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpf3: gpf3 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	gpf4: gpf4 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	uart0-bus {
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pins = "gpb0-3","gpb0-2", "gpb0-1", "gpb0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
	};

	uart1-bus {
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pins = "gpd2-3","gpd2-2", "gpd2-1", "gpd2-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
	};

	decon-te-on {
		samsung,pins = "gpe0-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
	};

	decon-te-off {
		samsung,pins = "gpe0-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
	};

	hs-i2c1-bus {
		samsung,pins = "gpf3-0","gpf3-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	hs-i2c2-bus {
		samsung,pins = "gpf3-2","gpf3-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	hs-i2c3-bus {
		samsung,pins = "gpf0-1","gpf0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	hs-i2c4-bus {
		samsung,pins = "gpf1-1","gpf1-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	hs-i2c5-bus {
		samsung,pins = "gpf0-3","gpf0-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	hs-i2c6-bus {
		samsung,pins = "gpf2-1","gpf2-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c0_bus: i2c0-bus {
		samsung,pins = "gpc1-1","gpc1-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c1_bus: i2c1-bus {
		samsung,pins = "gpc1-3","gpc1-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c4_bus: i2c4-bus {
		samsung,pins = "gpc4-1","gpc4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c5_bus: i2c5-bus {
		samsung,pins = "gpc4-3","gpc4-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c6_bus: i2c6-bus {
		samsung,pins = "gpc5-1","gpc5-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c7_bus: i2c7-bus {
		samsung,pins = "gpc8-1","gpc8-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	i2c8_bus: i2c8-bus {
		samsung,pins = "gpc9-1","gpc9-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi0-bus {
		samsung,pins = "gpc6-0","gpc6-2", "gpc6-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi0-bus-suspend {
		samsung,pins = "gpc6-0","gpc6-2", "gpc6-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi0-cs {
		samsung,pins = "gpc6-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi2-bus {
		samsung,pins = "gpc4-2","gpc5-0", "gpc5-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi2-cs-func {
		samsung,pins = "gpc4-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi2-cs {
		samsung,pins = "gpc4-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi3-bus {
		samsung,pins = "gpf3-3","gpf3-2", "gpf3-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi3-cs-0 {
		samsung,pins = "gpf3-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi3-cs-1 {
		samsung,pins = "gpd1-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi4-bus {
		samsung,pins = "gpf4-3","gpf4-2", "gpf4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi4-cs-0 {
		samsung,pins = "gpf4-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	spi4-cs-1 {
		samsung,pins = "gpd1-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	pwm-tout0 {
		samsung,pins = "gpc0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	pwm-tout1 {
		samsung,pins = "gpc0-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	fimc-is-mclk0-in {
		samsung,pins = "gpe0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	fimc-is-mclk1-fn {
		samsung,pins = "gpe0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR2>;
	};

	fimc-is-mclk0-out {
		samsung,pins = "gpe0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	fimc-is-mclk1-out {
		samsung,pins = "gpe0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	fimc-is-mclk2-out {
		samsung,pins = "gpf4-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
		samsung,pin-val = <0>;
	};

	fimc-is-mclk0-fn {
		samsung,pins = "gpe0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR2>;
	};

	fimc-is-mclk1-in {
		samsung,pins = "gpe0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	fimc-is-mclk2-fn {
		samsung,pins = "gpf4-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR3>;
	};

	fimc-is-flash {
		samsung,pins = "gpd3-2","gpd3-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	bt-en {
		samsung,pins = "gpd4-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
	};

	nfc-n5-firm {
		samsung,pins = "gpd4-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-val = <1>;
	};

	nfc-pvdd-en {
		samsung,pins = "gpd2-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	ese-pvdd-en {
		samsung,pins = "gpf4-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	spi2-mosi-sck-ssn {
		samsung,pins = "gpc5-1","gpc4-2", "gpc4-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	spi2-miso {
		samsung,pins = "gpc5-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	hs-i2c2-2-bus {
		samsung,pins = "gpf4-2","gpf4-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_3>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};

&pinctrl7 {
	gpc3: gpc3 {
		gpio-controller;
		#gpio-cells = <2>;

		interrupt-controller;
		#interrupt-cells = <2>;
	};

	i2c3_bus: i2c3-bus {
		samsung,pins = "gpc3-1","gpc3-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	ese-cs-func {
		samsung,pins = "gpc3-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
		samsung,pin-val = <1>;
	};

	ese-cs-func-suspend {
		samsung,pins = "gpc3-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};
