// SPDX-License-Identifier: GPL-2.0
/*
 * Samsung Galaxy A6 (2016) (gtaxlwifi) device tree source
 *
 * Copyright (c) ???
 */

/dts-v1/;
#include "exynos7870.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>

/ {
	model = "Samsung Galaxy Tab A6 (2016)";
	compatible = "samsung,gtaxlwifi", "samsung,exynos7870";
	chassis-type = "tablet";

	chosen {
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <1>;
		ranges;

		mfc_nfw@AE60000 {
			reg = <0 0xae600000 0x200000>;
		};

		mfc_fw@AE800000 {
			reg = <0 0xae800000 0x200000>;
		};

		bootloaderfb@67000000 {
			reg = <0 0x67000000 0x7e9000>;
			no-map;
		};
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0 0x40000000 0x3e400000>;
	};

	memory@80000000 {
		device_type = "memory";
		reg = <0 0x80000000 0x80000000>;
	};

	bootloaderfb@67000000 {
		compatible = "simple-framebuffer";
		reg = <0 0x67000000 (1200*1920*4)>;
		width = <1200>;
		height = <1920>;
		stride = <1920*4>;
		format =a8r8g8b8";
	};

	gpio-keys {
		compatible = "gpio-keys";
		pinctrl-names = "default";
		pinctrl-0 = <&key_power &key_volup &key_voldown &key_home>;

		key-power {
			label = "Power Key";
			gpios = <&gpa0 0 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_POWER>;
	        };

		key-volup {
			label = "Volume Up Key";
			gpios = <&gpa2 0 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEUP>;
		};

		key-voldown {
			label = "Volume Down Key";
			gpios = <&gpa2 1 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEDOWN>;
		};

		key-home {
			label = "Home Key";
			gpios = <&gpa1 7 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_HOMEPAGE>;
		};
	};

	vibrator {
		compatible = "regulator-haptic";
		haptic-supply = <&vdd_ldo32>;
		min-microvolt = <3300000>;
		max-microvolt = <3300000>;
	};

	vdd_fixed_mmc2: fixedregulator-mmc2 {
		compatible = "regulator-fixed";
		regulator-name = "vdd_fixed_mmc2";
		regulator-max-microvolt = <2800000>;
		regulator-min-microvolt = <2800000>;

		gpio = <&gpc0 0 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vdd_dummy_touch: dummyregulator-touch {
		compatible = "regulator-fixed";
		regulator-name = "vdd_dummy_touch";
	};

	vdd_dummy_mmc1: dummyregulator-mmc1 {
		compatible = "regulator-fixed";
		regulator-name = "vdd_dummy_mmc1";
	};
};

&pinctrl0 {
	cd_ext_irq: dwmmc2-cd-ext-irq {
		samsung,pins = "gpa0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	pmic_irq: pmic-irq {
		samsung,pins = "gpa0-2";
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	tkey-irq {
		samsung,pins = "gpa1-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	tkey-input {
		samsung,pins = "gpa1-4";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	key_power: key-power {
		samsung,pins = "gpa0-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	key_volup: key-volup {
		samsung,pins = "gpa2-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	key_voldown: key-voldown {
		samsung,pins = "gpa2-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	key_home: key-home {
		samsung,pins = "gpa1-7";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	btp-irq {
		samsung,pins = "gpa0-7";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	prox-int {
		samsung,pins = "gpa0-5";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	wlan_enable: wlan-enable {
		samsung,pins = "gpa2-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
	};

	acc-int {
		samsung,pins = "gpa2-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	grip-irq {
		samsung,pins = "gpa1-5";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	if-irq {
		samsung,pins = "gpa2-7";
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	if-pmic-irq {
		samsung,pins = "gpa2-7";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	cod3026-irq {
		samsung,pins = "gpa0-4";
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	fuel_irq: fuel-irq {
		samsung,pins = "gpa0-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};

&pinctrl2 {
	btp-irq {
		samsung,pins = "gpa0-7";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};
};

&pinctrl6 {
	pm_wrsti: pm-wrsti {
		samsung,pins = "gpd1-0";
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
	};

	btp-ldo {
		samsung,pins = "gpd4-5";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_DOWN>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	btp-reset {
		samsung,pins = "gpd4-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};

	cfg-wlanen {
		samsung,pins = "gpd3-6";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
		samsung,pin-val = <0>;
	};

	cnss-wlanen {
		samsung,pins = "gpd3-6";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
		samsung,pin-val = <0>;
	};

	ds-det {
		samsung,pins = "gpd2-3";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	proxy-i2c {
		samsung,pins = "gpc5-1", "gpc5-0";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	sensor-i2c {
		samsung,pins = "gpc4-3", "gpc4-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};

	grip-i2c-bus {
		samsung,pins = "gpc1-3", "gpc1-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_2>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
	};
};

&pinctrl7 {
	attn-input {
		samsung,pins = "gpc3-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};

	attn_irq: attn-irq {
		samsung,pins = "gpc3-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};
};

&i2c3 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	samsung,i2c-max-bus-freq = <400000>;
	samsung,i2c-sda-delay = <100>;

	touchscreen@70 {
		compatible = "syna,rmi4-i2c";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <0x70>;
		interrupt-parent = <&gpc3>;
		interrupts = <2 IRQ_TYPE_EDGE_FALLING>;

		pinctrl-names = "default";
		pinctrl-0 = <&attn_irq>;

		vdd-supply = <&vdd_dummy_touch>;
		vio-supply = <&vdd_ldo29>;

		syna,reset-delay-ms = <200>;
		syna,startup-delay-ms = <200>;

		rmi4-f01@1 {
			reg = <0x01>;
			syna,nosleep-mode = <1>;
		};

		rmi4-f12@12 {
			reg = <0x12>;
			syna,sensor-type = <1>;
			syna,rezero-wait-ms = <200>;
		};
	};
};

&i2c4 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	samsung,i2c-max-bus-freq = <400000>;
	samsung,i2c-sda-delay = <100>;

	fuelgauge@3b {
		compatible = "samsung,s2mu005-fg";
		reg = <0x3b>;
		interrupt-parent = <&gpa0>;
		interrupts = <3 IRQ_TYPE_EDGE_FALLING>;

		pinctrl-names = "default";
		pinctrl-0 = <&fuel_irq>;
	};
};

&i2c7 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	samsung,i2c-max-bus-freq = <400000>;
	samsung,i2c-sda-delay = <100>;

	touchkey@49 {
		compatible = "melfas,mip4_ts";
		reg = <0x49>;
		interrupt-parent = <&gpa1>;
		interrupts = <4 IRQ_TYPE_LEVEL_LOW>;

		mip4_ts,keycode = <KEY_CYCLEWINDOWS KEY_BACK>;
	};
};

&hsi2c0 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	sda-gpios = <&gpm0 0 GPIO_ACTIVE_HIGH>;
	scl-gpios = <&gpm0 1 GPIO_ACTIVE_HIGH>;
	i2c-gpio,delay-us = <2>;

	s2mpu05-pmic@66 {
		compatible = "samsung,s2mpu05-pmic";
		reg = <0x66>;
		interrupt-parent = <&gpa0>;
		interrupts = <2 IRQ_TYPE_LEVEL_LOW>;

		pinctrl-names = "default";
		pinctrl-0 = <&pmic_irq &pm_wrsti>;

		regulators {
			BUCK1 {
				regulator-name = "BUCK1";
				regulator-min-microvolt = <500000>;
				regulator-max-microvolt = <1300000>;
				regulator-ramp-delay = <12000>;

				regulator-expected-consumer = <2>;

				regulator-always-on;
				regulator-boot-on;
			};

			BUCK2 {
				regulator-name = "BUCK2";
				regulator-min-microvolt = <500000>;
				regulator-max-microvolt = <1300000>;
				regulator-ramp-delay = <12000>;

				regulator-expected-consumer = <4>;

				regulator-always-on;
				regulator-boot-on;
			};

			BUCK3 {
				regulator-name = "BUCK3";
				regulator-min-microvolt = <500000>;
				regulator-max-microvolt = <1300000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			BUCK4 {
				regulator-name = "BUCK4";
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1500000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			BUCK5 {
				regulator-name = "BUCK5";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <2100000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo1: LDO1 {
				regulator-name = "vdd_ldo1";
				regulator-min-microvolt = <650000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo2: LDO2 {
				regulator-name = "vdd_ldo2";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <2800000>;
				regulator-ramp-delay = <12000>;
			};

			vdd_ldo3: LDO3 {
				regulator-name = "vdd_ldo3";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo4: LDO4 {
				regulator-name = "vdd_ldo4";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo5: LDO5 {
				regulator-name = "vdd_ldo5";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo6: LDO6 {
				regulator-name = "vdd_ldo6";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo7: LDO7 {
				regulator-name = "vdd_ldo7";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo8: LDO8 {
				regulator-name = "vdd_ldo8";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3375000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo9: LDO9 {
				regulator-name = "vdd_ldo9";
				regulator-min-microvolt = <650000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo10: LDO10 {
				regulator-name = "vdd_ldo10";
				regulator-min-microvolt = <650000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
			};

			vdd_ldo25: LDO25 {
				regulator-name = "vdd_ldo25";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;
			};

			vdd_ldo26: LDO26 {
				regulator-name = "vdd_ldo26";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3375000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo27: LDO27 {
				regulator-name = "vdd_ldo27";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo28: LDO28 {
				regulator-name = "vdd_ldo28";
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
			};

			vdd_ldo29: LDO29 {
				regulator-name = "vdd_ldo29";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-ramp-delay = <12000>;
			};

			vdd_ldo31: LDO31 {
				regulator-name = "vdd_ldo31";
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo32: LDO32 {
				regulator-name = "vdd_ldo32";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-ramp-delay = <12000>;
			};

			vdd_ldo33: LDO33 {
				regulator-name = "vdd_ldo33";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-ramp-delay = <12000>;
			};

			vdd_ldo34: LDO34 {
				regulator-name = "vdd_ldo34";
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
				regulator-boot-on;
			};

			vdd_ldo35: LDO35 {
				regulator-name = "vdd_ldo35";
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
			};
		};
	};
};

&gpu {
	status = "okay";
};

&dwmmc0 {
	status = "okay";
	non-removable;

	vmmc-supply = <&vdd_ldo26>;
	vqmmc-supply = <&vdd_ldo27>;

	fifo-depth = <64>;
	samsung,dw-mshc-ciu-div = <3>;
	samsung,dw-mshc-sdr-timing = <0 4>;
	samsung,dw-mshc-ddr-timing = <2 4>;
};

&dwmmc1 {
	status = "okay";
	non-removable;
	fifo-access-32bit;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	cap-sdio-irq;
	cap-sd-highspeed;

	pinctrl-names = "default";
	pinctrl-0 = <&sd1_clk &sd1_cmd &sd1_bus1 &sd1_bus4>;

	vmmc-supply = <&wlan_enable>;
	vqmmc-supply = <&vdd_dummy_mmc1>;

	bus-width = <4>;
	fifo-depth = <64>;
	samsung,dw-mshc-ciu-div = <3>;
	samsung,dw-mshc-sdr-timing = <0 3>;
	samsung,dw-mshc-ddr-timing = <1 2>;
	samsung,dw-mshc-sdr50-timing = <2 4>;
	samsung,dw-mshc-sdr104-timing = <0 3>;
};

&dwmmc2 {
	status = "okay";
	non-removable;
	disable-wp;

	pinctrl-names = "default";
	pinctrl-0 = <&sd2_clk &sd2_cmd &sd2_bus1 &sd2_bus4 &cd_ext_irq>;

	vmmc-supply = <&vdd_fixed_mmc2>;
	vqmmc-supply = <&vdd_ldo2>;

	bus-width = <4>;
	fifo-depth = <64>;
	samsung,dw-mshc-ciu-div = <3>;
	samsung,dw-mshc-sdr-timing = <0 3>;
	samsung,dw-mshc-ddr-timing = <1 2>;
};

&usbdrd {
	status = "okay";
};
